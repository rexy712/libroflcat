#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#Copyright 2018-2019 rexy712

#Makefile to generate a single static or shared library from all the sources in SOURCE_DIRS ending in EXT

ifeq ($(OS),Windows_NT)
	WINDOWS::=1
endif

SOURCE_DIRS::=src
OBJDIR::=obj
DEPDIR::=$(OBJDIR)/dep
LIBDIR::=lib
INCLUDE_DIRS::=include
CFLAGS::=-g -std=c18 -Wall -pedantic -Wextra
CXXFLAGS::=-g -std=c++17 -Wall -pedantic -Wextra
EXT::=c
LANG::=$(EXT)
MAIN_LIBRARY::=roflcat
DLLOUT::=$(MAIN_LIBRARY).dll
SHARED?=1
RELEASE?=0
MEMCHK?=0

ifneq ($(WINDOWS),1)
	#*nix settings
	CC::=gcc
	CXX::=g++
	LDLIBS::=
	LDFLAGS::=
	STRIP::=strip
	RANLIB::=ranlib
	AR::=ar
	AS::=as
else #windows
	#windows is a fuckwit
	#windows settings
	MINGW_PREFIX::=x86_64-w64-mingw32-
	CC::=$(MINGW_PREFIX)gcc
	CXX::=$(MINGW_PREFIX)g++
	LDLIBS::=
	LDFLAGS::=-static-libgcc -static-libstdc++
	STRIP::=$(MINGW_PREFIX)strip
	RANLIB::=$(MINGW_PREFIX)ranlib
	AR::=$(MINGW_PREFIX)ar
	AS::=$(MINGW_PREFIX)as
endif #windows

ifeq ($(SHARED),1)
	ifeq ($(WINDOWS),1)
		INTERNAL_MAIN_LIBRARY::=lib$(MAIN_LIBRARY).a
	else
		INTERNAL_MAIN_LIBRARY::=lib$(MAIN_LIBRARY).so
	endif
else
	INTERNAL_MAIN_LIBRARY::=lib$(MAIN_LIBRARY).a
endif


#system dependant bullshit
ifeq ($(OS),Windows_NT)
	#windows' cmd commands
	mkdir=mkdir $(subst /,\,$(1)) > NUL 2>&1
	rm=del /F $(1) > NUL 2>&1
	rmdir=rd /S /Q $(1) > NUL 2>&1
	move=move /Y $(subst /,\,$(1)) $(subst /,\,$(2)) > NUL 2>&1
	copy=copy /Y /B $(subst /,\,$(1)) $(subst /,\,$(2)) > NUL 2>&1
else
	#*nix terminal commands
	mkdir=mkdir -p $(1)
	rm=rm -f $(1)
	rmdir=rm -rf $(1)
	move=mv $(1) $(2)
	copy=cp $(1) $(2)
endif

#setup compiler and flags based on language
ifeq ($(LANG),cpp)
	COMPILER_FLAGS::=$(CXXFLAGS)
	COMPILER::=$(CXX)
else ifeq ($(LANG),c)
	COMPILER_FLAGS::=$(CFLAGS)
	COMPILER::=$(CC)
endif

ifeq ($(RELEASE),1)
	#a lot of false strict aliasing warnings from gcc 9
	COMPILER_FLAGS+=-O2 -Wno-strict-aliasing
else ifeq ($(MEMCHK),1)
	#use asan to check memory leaks/invalid accesses
	LDFLAGS+=-fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls
	COMPILER_FLAGS+=-O0 -g3 -ggdb -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls
else
	#default target
	COMPILER_FLAGS+=-O0 -g3 -ggdb
endif

INTERNAL_COMPILERFLAGS=-c $(foreach dir,$(INCLUDE_DIRS),-I"$(dir)") -MMD -MP -MF"$(DEPDIR)/$(notdir $(patsubst %.o,%.d,$@))"
ifeq ($(SHARED),1)
	INTERNAL_COMPILERFLAGS+=-fPIC
endif
SOURCES::=$(foreach source,$(SOURCE_DIRS),$(foreach ext,$(EXT),$(wildcard $(source)/*.$(ext))))
OBJECTS::=$(addprefix $(OBJDIR)/,$(subst \,.,$(subst /,.,$(addsuffix .o,$(SOURCES)))))

.PHONY: all

ifeq ($(SHARED),1)
ifeq ($(WINDOWS),1)
#target for windows shared library
all: $(DLLOUT)
$(INTERNAL_MAIN_LIBRARY): $(OBJECTS)
	$(COMPILER) -shared -o "$(DLLOUT)" $^ -Wl,--out-implib,"lib$(MAIN_LIBRARY).a" $(LDLIBS) $(LDFLAGS)
$(DLLOUT): $(INTERNAL_MAIN_LIBRARY)
else #windows
#target for *nix shared library
all: $(INTERNAL_MAIN_LIBRARY)
$(INTERNAL_MAIN_LIBRARY): $(OBJECTS)
	$(COMPILER) -shared -o "$@" $^ $(COMPILER_FLAGS) $(LDFLAGS) $(LDLIBS)
endif #windows
else #shared
#target for static library
all: $(INTERNAL_MAIN_LIBRARY)
$(INTERNAL_MAIN_LIBRARY): $(OBJECTS)
	$(AR) rcs "$@" $^
	$(RANLIB) "$@"
endif #shared

#Object target recipe
define GENERATE_OBJECTS
$$(OBJDIR)/$(subst \,.,$(subst /,.,$(1))).%.o: $(1)/%
	$$(COMPILER) $$(COMPILER_FLAGS) $$(INTERNAL_COMPILERFLAGS) "$$<" -o "$$@"
endef

#Create targets for object files
$(foreach dir,$(SOURCE_DIRS),$(eval $(call GENERATE_OBJECTS,$(dir))))
$(OBJECTS): | $(OBJDIR) $(DEPDIR)

$(OBJDIR):
	$(call mkdir,"$@")
$(DEPDIR):
	$(call mkdir,"$@")

.PHONY: clean
clean:
	$(call rmdir,"$(DEPDIR)")
	$(call rmdir,"$(OBJDIR)")
	$(call rm,"lib$(MAIN_LIBRARY).so")
	$(call rm,"lib$(MAIN_LIBRARY).a")
	$(call rm,"$(DLLOUT)")

#header file dep tracking
-include $(wildcard $(DEPDIR)/*.d)

