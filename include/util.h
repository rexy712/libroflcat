/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ROFLCAT_UTIL_H
#define ROFLCAT_UTIL_H

#include <wchar.h> //wchar_t

#ifdef __cplusplus
extern "C"{
#endif

static inline int _roflcat_abs(int a){
	return (a < 0) ? -a : a;
}
static inline int _roflcat_min(int a, int b){
	return (a > b) ? b : a;
}
static inline int _roflcat_max(int a, int b){
	return (a > b) ? a : b;
}
static inline void _roflcat_to_caret_notation(char c, char* dest){
	dest[0] = '^';
	dest[1] = c ^ 0x40;
	dest[2] = 0;
}
static inline void _roflcat_to_mdash_notation(char c, char* dest){
	int corin = c & 0x7F;
	dest[0] = 'M';
	dest[1] = '-';
	if(corin > 32 && corin < 127){
		dest[2] = corin;
		dest[3] = 0;
	}else{
		_roflcat_to_caret_notation(corin, dest+2);
	}
}
static inline void _roflcat_nonprinting_notation(char c, char* dest){
	if(c & 0x80){
		_roflcat_to_mdash_notation(c, dest);
	}else if(c == '\n' || c == '\t' || (c > 31 && c < 127)){
		dest[0] = c;
		dest[1] = 0;
	}else{
		_roflcat_to_caret_notation(c, dest);
	}
}


#ifdef __cplusplus
}
#endif

#endif
