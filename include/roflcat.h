/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ROFLCAT_PRINTER_H
#define ROFLCAT_PRINTER_H

#include <wchar.h> //wchar_t

#ifdef __cplusplus
extern "C"{
#endif

enum roflcat_printer_type{
	ROFLCAT_PRINTER_TYPE_LOOKUP,
	ROFLCAT_PRINTER_TYPE_MONO,
	ROFLCAT_PRINTER_TYPE_RGB,
	ROFLCAT_PRINTER_TYPE_CUSTOM
};
enum roflcat_truecolor_target{
	ROFLCAT_TC_TARGET_R,
	ROFLCAT_TC_TARGET_G,
	ROFLCAT_TC_TARGET_B,
};

typedef struct ROFLCAT_color{
	int r, g, b;
	enum roflcat_truecolor_target target;
}ROFLCAT_color;

//Don't trust this structure to stay consistent. Use the API instead
struct ROFLCAT_printer{
	union{
		//lookup table based colors
		struct{
			const int* table;   //lookup table
			int table_size;     //size of lookup table
			int column;         //current column of output
			int line;           //current line of output
			int index;          //current index into the table
		}lookup;
		//truecolor style colors
		struct{
			ROFLCAT_color color,      //current color
			              line_color; //color at start of line
			int color_min;            //minimum value of colors
			int color_max;            //maximum value of colors
		}truecolor;
		void* custom_data; //used with ROFLCAT_PRINT_TYPE_CUSTOM
	};
	//common internal state tracking
	const wchar_t* reset;           //string to use for color resetting
	const wchar_t* color_string;    //string to use for color setting
	enum roflcat_printer_type type; //printer type for how to process output
	int line_num;                   //used for line number printing
	unsigned is_squeezing:1;        //currently suppressing newline output when squeeze_blanks==1
	unsigned had_newline:1;         //last character was a newline

	//flags
	unsigned invert:1;           //change background instead of foreground color
	unsigned number_lines:2;     //print line numbering
	unsigned squeeze_blanks:1;   //multiple newlines in a row are eliminated
	unsigned show_ends:1;        //print line_end_str at the end of a line
	unsigned show_nonprinting:1; //treat the characters as pure ascii
	float freq;                  //how fast the color changes on a line
	float spread;                //how fast the color changes between lines
	int randomness;              //a randomness salt

	const wchar_t* line_end_str; //string to print at end of line when show_ends==1
  const wchar_t* tab_str;      //string to print for tab character

};
typedef struct ROFLCAT_printer ROFLCAT_printer;

enum roflcat_number_lines{
	ROFLCAT_NUMBER_LINES_NEVER = 0,
	ROFLCAT_NUMBER_LINES_ALWAYS = 1,
	ROFLCAT_NUMBER_LINES_NONBLANK = 2,
};

typedef int(*roflcat_print_fun)(ROFLCAT_printer*, wchar_t);

const wchar_t* roflcat_lookup_invert_color_string(void);
const wchar_t* roflcat_lookup_color_string(void);
const wchar_t* roflcat_lookup_color_reset(void);
const wchar_t* roflcat_rgb_invert_color_string(void);
const wchar_t* roflcat_rgb_color_string(void);
const wchar_t* roflcat_rgb_color_reset(void);
const int* roflcat_256_color_table(void);
int roflcat_256_color_table_len(void);
const int* roflcat_16_color_table(void);
int roflcat_16_color_table_len(void);
const int* roflcat_8_color_table(void);
int roflcat_8_color_table_len(void);
void roflcat_default_init_locale(void);

void roflcat_init_printer_mono(ROFLCAT_printer* p);
void roflcat_init_printer_lookup(ROFLCAT_printer* p, const int* table, int tablelen, int randomness);
void roflcat_init_printer_256(ROFLCAT_printer* p, int randomness);
void roflcat_init_printer_16(ROFLCAT_printer* p, int randomness);
void roflcat_init_printer_8(ROFLCAT_printer* p, int randomness);
void roflcat_init_printer_rgb(ROFLCAT_printer* p, int randomness);
void roflcat_init_printer_custom(ROFLCAT_printer* p, void* data);


void roflcat_set_freq(ROFLCAT_printer* p, float newf);
void roflcat_set_spread(ROFLCAT_printer* p, float news);
void roflcat_set_randomness(ROFLCAT_printer* p, int rd);
void roflcat_set_invert(ROFLCAT_printer* p, int b);
void roflcat_set_show_ends(ROFLCAT_printer* p, int b);
void roflcat_set_number_lines(ROFLCAT_printer* p, int b);
void roflcat_set_squeeze_blanks(ROFLCAT_printer* p, int b);
void roflcat_set_show_nonprinting(ROFLCAT_printer* p, int b);
void roflcat_set_line_end_str(ROFLCAT_printer* p, const wchar_t* line_ending);
void roflcat_set_tab_str(ROFLCAT_printer* p, const wchar_t* tab_str);
void roflcat_set_line_number(ROFLCAT_printer* p, int num);
void roflcat_custom_set_color_string(ROFLCAT_printer* p, const wchar_t* str);
void roflcat_custom_set_color_reset(ROFLCAT_printer* p, const wchar_t* str);
void roflcat_lookup_set_table(ROFLCAT_printer* p, const int* table, int tablelen);
void roflcat_rgb_set_color_min(ROFLCAT_printer* p, int n);
void roflcat_rgb_set_color_max(ROFLCAT_printer* p, int n);
void roflcat_rgb_recolor(ROFLCAT_printer* p);

int roflcat_get_freq(const ROFLCAT_printer* p);
int roflcat_get_spread(const ROFLCAT_printer* p);
int roflcat_get_randomness(const ROFLCAT_printer* p);
int roflcat_get_invert(const ROFLCAT_printer* p);
int roflcat_get_show_ends(const ROFLCAT_printer* p);
enum roflcat_number_lines roflcat_get_number_lines(const ROFLCAT_printer* p);
int roflcat_get_squeeze_blanks(const ROFLCAT_printer* p);
int roflcat_get_show_nonprinting(const ROFLCAT_printer* p);
const wchar_t* roflcat_get_line_end_str(const ROFLCAT_printer* p);
const wchar_t* roflcat_get_tab_str(const ROFLCAT_printer* p);
int roflcat_get_line_number(const ROFLCAT_printer* p);
const wchar_t* roflcat_get_color_string(const ROFLCAT_printer* p);
const wchar_t* roflcat_get_color_reset(const ROFLCAT_printer* p);
const int* roflcat_lookup_get_table(const ROFLCAT_printer* p);
int roflcat_rgb_get_color_min(const ROFLCAT_printer* p);
int roflcat_rgb_get_color_max(const ROFLCAT_printer* p);

int roflcat_print_wchar(ROFLCAT_printer* p, wchar_t c, roflcat_print_fun print_fn);
int roflcat_print_wstring(ROFLCAT_printer* p, const wchar_t* str, roflcat_print_fun custom_fn);
int roflcat_print_char(ROFLCAT_printer* p, char c, roflcat_print_fun print_fn);
int roflcat_print_string(ROFLCAT_printer* p, const char* str, roflcat_print_fun custom_fn);
void roflcat_reset_color(ROFLCAT_printer* p);

#ifdef __cplusplus
}
#endif

#endif
