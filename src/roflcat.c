/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _XOPEN_SOURCE /*wcwidth*/
#include <roflcat.h>
#include "util.h"

#include <wchar.h> //wprintf, wchar_t
#include <stdio.h> //wprintf
#include <locale.h> //set_locale, LC_ALL


#define UNUSED_VARIABLE(x) ((void)(x))

static inline void _roflcat_set_color(ROFLCAT_color* c, int r, int g, int b, enum roflcat_truecolor_target t){
	c->r = r;
	c->g = g;
	c->b = b;
	c->target = t;
}
static inline void _roflcat_copy_color(ROFLCAT_color* restrict dest, const ROFLCAT_color* restrict src){
	dest->r = src->r;
	dest->g = src->g;
	dest->b = src->b;
	dest->target = src->target;
}

#define ROFLCAT_DEFAULT_FREQ               0.3
#define ROFLCAT_DEFAULT_SPREAD             0.4
#define ROFLCAT_DEFAULT_TAB_SIZE           4
#define ROFLCAT_DEFAULT_INVERT             0
#define ROFLCAT_DEFAULT_NUMBER_LINES       ROFLCAT_NUMBER_LINES_NEVER
#define ROFLCAT_DEFAULT_SQUEEZE_BLANKS     0
#define ROFLCAT_DEFAULT_SHOW_ENDS          0
#define ROFLCAT_DEFAULT_SHOW_NONPRINTING   0
#define ROFLCAT_DEFAULT_RANDOMNESS         0
#define ROFLCAT_DEFAULT_LINE_END_STR       L"$";
#define ROFLCAT_DEFAULT_TAB_STR            L"\t";
#define ROFLCAT_DEFAULT_LOOKUP_TAB_STR     L"        ";

#define ROFLCAT_DEFAULT_TRUECOLOR_MIN_COLOR 20
#define ROFLCAT_DEFAULT_TRUECOLOR_MAX_COLOR 255

#define ROFLCAT_STRINGIFY(x) ROFLCAT_REAL_STRINGIFY(x)
#define ROFLCAT_REAL_STRINGIFY(x) #x
#define ROFLCAT_LINE_NUMBERING_SPACE ROFLCAT_STRINGIFY(8)

//Default tables
static const wchar_t* _roflcat_lookup_color_string_invert = L"\033[38;5;0m\033[48;5;%sm";
static const wchar_t* _roflcat_lookup_color_string = L"\033[38;5;%sm";
static const wchar_t* _roflcat_rgb_color_string_invert = L"\033[38;5;0m\033[48;2;%s;%s;%sm";
static const wchar_t* _roflcat_rgb_color_string = L"\033[38;2;%s;%s;%sm";
static const wchar_t* _roflcat_lookup_color_reset = L"\033[0m";
static const int _roflcat_256_color_table[] = {39, 38, 44, 43, 49, 48, 84, 83, 119, 118, 154, 148, 184, 178, 214, 208, 209, 203, 204, 198, 199, 163, 164, 128, 129, 93, 99, 63, 69, 33};
static const int _roflcat_16_color_table[] = {12, 12, 14, 14, 10, 10, 11, 11, 9, 9, 13, 13};
static const char* _roflcat_rgb_lookup_table[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", "181", "182", "183", "184", "185", "186", "187", "188", "189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200", "201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220", "221", "222", "223", "224", "225", "226", "227", "228", "229", "230", "231", "232", "233", "234", "235", "236", "237", "238", "239", "240", "241", "242", "243", "244", "245", "246", "247", "248", "249", "250", "251", "252", "253", "254", "255"};

const wchar_t* roflcat_lookup_color_string(void){
	return _roflcat_lookup_color_string;
}
const wchar_t* roflcat_lookup_invert_color_string(void){
	return _roflcat_lookup_color_string_invert;
}
const wchar_t* roflcat_lookup_color_reset(void){
	return _roflcat_lookup_color_reset;
}
const wchar_t* roflcat_rgb_invert_color_string(void){
	return _roflcat_rgb_color_string_invert;
}
const wchar_t* roflcat_rgb_color_string(void){
	return _roflcat_rgb_color_string;
}
const wchar_t* roflcat_rgb_color_reset(void){
	return _roflcat_lookup_color_reset;
}
const int* roflcat_256_color_table(void){
	return _roflcat_256_color_table;
}
int roflcat_256_color_table_len(void){
  return sizeof(_roflcat_256_color_table) / sizeof(_roflcat_256_color_table[0]);
}
const int* roflcat_16_color_table(void){
	return _roflcat_16_color_table;
}
int roflcat_16_color_table_len(void){
	return sizeof(_roflcat_16_color_table) / sizeof(_roflcat_16_color_table[0]);
}
const int* roflcat_8_color_table(void){
	return roflcat_16_color_table();
}
int roflcat_8_color_table_len(void){
	return roflcat_16_color_table_len();
}

//Init API
void roflcat_default_init_locale(void){
	setlocale(LC_ALL, "");
}
static void _roflcat_init_printer(ROFLCAT_printer* p){
	p->freq = ROFLCAT_DEFAULT_FREQ;
	p->spread = ROFLCAT_DEFAULT_SPREAD;
	p->line_num = 0;
	p->randomness = ROFLCAT_DEFAULT_RANDOMNESS;
	p->is_squeezing = 0;
	p->had_newline = 1;

	p->invert = ROFLCAT_DEFAULT_INVERT;
	p->number_lines = ROFLCAT_NUMBER_LINES_NEVER;
	p->squeeze_blanks = ROFLCAT_DEFAULT_SQUEEZE_BLANKS;
	p->show_ends = ROFLCAT_DEFAULT_SHOW_ENDS;
	p->show_nonprinting = ROFLCAT_DEFAULT_SHOW_NONPRINTING;
	p->line_end_str = ROFLCAT_DEFAULT_LINE_END_STR;
  p->tab_str = ROFLCAT_DEFAULT_TAB_STR;
}
void roflcat_init_printer_mono(ROFLCAT_printer* p){
	_roflcat_init_printer(p);
	p->type = ROFLCAT_PRINTER_TYPE_MONO;
}
void roflcat_init_printer_lookup(ROFLCAT_printer* p, const int* table, int tablelen, int randomness){
	_roflcat_init_printer(p);
	p->lookup.table = table;
	p->lookup.table_size = tablelen;
	p->lookup.column = 0;
	p->lookup.line = 0;
	p->lookup.index = -1;
  p->tab_str = ROFLCAT_DEFAULT_LOOKUP_TAB_STR;
	p->reset = roflcat_lookup_color_reset();
	p->color_string = roflcat_lookup_color_string();
	p->type = ROFLCAT_PRINTER_TYPE_LOOKUP;
	p->randomness = randomness % p->lookup.table_size;
}
void roflcat_init_printer_256(ROFLCAT_printer* p, int randomness){
	roflcat_init_printer_lookup(p, roflcat_256_color_table(), roflcat_256_color_table_len(), randomness);
}
void roflcat_init_printer_16(ROFLCAT_printer* p, int randomness){
	roflcat_init_printer_lookup(p, roflcat_16_color_table(), roflcat_16_color_table_len(), randomness);
}
void roflcat_init_printer_8(ROFLCAT_printer* p, int randomness){
	roflcat_init_printer_lookup(p, roflcat_8_color_table(), roflcat_8_color_table_len(), randomness);
}
void roflcat_init_printer_rgb(ROFLCAT_printer* p, int randomness){
	_roflcat_init_printer(p);
	p->truecolor.color_min = ROFLCAT_DEFAULT_TRUECOLOR_MIN_COLOR;
	p->truecolor.color_max = ROFLCAT_DEFAULT_TRUECOLOR_MAX_COLOR;

	p->reset = roflcat_rgb_color_reset();
	p->color_string = roflcat_rgb_color_string();
	p->type = ROFLCAT_PRINTER_TYPE_RGB;
	p->randomness = randomness;
	roflcat_rgb_recolor(p);
}
void roflcat_init_printer_custom(ROFLCAT_printer* p, void* data){
	_roflcat_init_printer(p);
	p->custom_data = data;
	p->type = ROFLCAT_PRINTER_TYPE_CUSTOM;
}

//Setter API
void roflcat_set_freq(ROFLCAT_printer* p, float newf){
	p->freq = newf;
}
void roflcat_set_spread(ROFLCAT_printer* p, float news){
	p->spread = news;
}
void roflcat_set_randomness(ROFLCAT_printer* p, int rd){
	p->randomness = rd;
	if(p->type == ROFLCAT_PRINTER_TYPE_LOOKUP){
		p->randomness %= p->lookup.table_size;
	}
}
void roflcat_set_invert(ROFLCAT_printer* p, int b){
	p->invert = b;
	switch(p->type){
	case ROFLCAT_PRINTER_TYPE_LOOKUP:
		p->color_string = b ? roflcat_lookup_invert_color_string() : roflcat_lookup_color_string();
		break;
	case ROFLCAT_PRINTER_TYPE_RGB:
		p->color_string = b ? roflcat_rgb_invert_color_string() : roflcat_rgb_color_string();
		break;
	default:
		break;
	};
}
void roflcat_set_show_ends(ROFLCAT_printer* p, int b){
	p->show_ends = b;
}
void roflcat_set_number_lines(ROFLCAT_printer* p, int b){
	p->number_lines = b;
}
void roflcat_set_squeeze_blanks(ROFLCAT_printer* p, int b){
	if(!b)
		p->is_squeezing = 0;
	p->squeeze_blanks = b;
}
void roflcat_set_show_nonprinting(ROFLCAT_printer* p, int b){
	p->show_nonprinting = b;
}
void roflcat_set_line_end_str(ROFLCAT_printer* p, const wchar_t* line_ending){
	p->line_end_str = line_ending;
}
void roflcat_set_tab_str(ROFLCAT_printer* p, const wchar_t* tab_str){
	p->tab_str = tab_str;
}
void roflcat_set_line_number(ROFLCAT_printer* p, int num){
	p->line_num = num;
}
void roflcat_custom_set_color_string(ROFLCAT_printer* p, const wchar_t* str){
	if(p->type == ROFLCAT_PRINTER_TYPE_CUSTOM){
		p->color_string = str;
	}
}
void roflcat_custom_set_color_reset(ROFLCAT_printer* p, const wchar_t* str){
	if(p->type == ROFLCAT_PRINTER_TYPE_CUSTOM){
		p->reset = str;
	}
}
void roflcat_lookup_set_table(ROFLCAT_printer* p, const int* table, int tablelen){
	p->lookup.table = table;
	p->lookup.table_size = tablelen;
	p->lookup.index = -1;
	roflcat_set_randomness(p, p->randomness);
}
void roflcat_rgb_set_color_min(ROFLCAT_printer* p, int n){
	p->truecolor.color_min = n;
}
void roflcat_rgb_set_color_max(ROFLCAT_printer* p, int n){
	p->truecolor.color_max = n;
}
void roflcat_rgb_recolor(ROFLCAT_printer* p){
	switch(p->randomness%3){
	case 0:
		_roflcat_set_color(&p->truecolor.color, p->randomness%(p->truecolor.color_max-p->truecolor.color_min) + p->truecolor.color_min,
		                                        p->truecolor.color_min,
		                                        p->truecolor.color_max,
		                                        ROFLCAT_TC_TARGET_R);
		break;
	case 1:
		_roflcat_set_color(&p->truecolor.color, p->truecolor.color_max,
			                                      p->randomness%(p->truecolor.color_max-p->truecolor.color_min) + p->truecolor.color_min,
		                                        p->truecolor.color_min,
		                                        ROFLCAT_TC_TARGET_G);
		break;
	case 2:
		_roflcat_set_color(&p->truecolor.color, p->truecolor.color_min,
		                                        p->truecolor.color_max,
			                                      p->randomness%(p->truecolor.color_max-p->truecolor.color_min) + p->truecolor.color_min,
		                                        ROFLCAT_TC_TARGET_B);
		break;
	};
	_roflcat_copy_color(&p->truecolor.line_color, &p->truecolor.color);
}
//Getter API
int roflcat_get_freq(const ROFLCAT_printer* p){
	return p->freq;
}
int roflcat_get_spread(const ROFLCAT_printer* p){
	return p->spread;
}
int roflcat_get_randomness(const ROFLCAT_printer* p){
	return p->randomness;
}
int roflcat_get_invert(const ROFLCAT_printer* p){
	return p->invert;
}
int roflcat_get_show_ends(const ROFLCAT_printer* p){
	return p->show_ends;
}
enum roflcat_number_lines roflcat_get_number_lines(const ROFLCAT_printer* p){
	return p->number_lines;
}
int roflcat_get_squeeze_blanks(const ROFLCAT_printer* p){
	return p->squeeze_blanks;
}
int roflcat_get_show_nonprinting(const ROFLCAT_printer* p){
	return p->show_nonprinting;
}
const wchar_t* roflcat_get_line_end_str(const ROFLCAT_printer* p){
	return p->line_end_str;
}
const wchar_t* roflcat_get_tab_str(const ROFLCAT_printer* p){
	return p->tab_str;
}
int roflcat_get_line_number(const ROFLCAT_printer* p){
	return p->line_num;
}
const wchar_t* roflcat_get_color_string(const ROFLCAT_printer* p){
	return p->color_string;
}
const wchar_t* roflcat_get_color_reset(const ROFLCAT_printer* p){
	return p->reset;
}
const int* roflcat_lookup_get_table(const ROFLCAT_printer* p){
	return p->lookup.table;
}
int roflcat_rgb_get_color_min(const ROFLCAT_printer* p){
	return p->truecolor.color_min;
}
int roflcat_rgb_get_color_max(const ROFLCAT_printer* p){
	return p->truecolor.color_max;
}

//Internal
static void _roflcat_lookup_inc_line(ROFLCAT_printer* p){
	++p->lookup.line;
	p->lookup.column = 0;
}
static void _roflcat_lookup_inc_col(ROFLCAT_printer* p, int num){
	p->lookup.column += num;
}
static void _roflcat_reset_color(const wchar_t* r){
	wprintf(L"%ls", r);
}
static void _roflcat_lookup_reset(ROFLCAT_printer* p){
	_roflcat_reset_color(p->reset);
	p->lookup.index = -1; //force update color on next character
}
static void _roflcat_rgb_reset(ROFLCAT_printer* p){
	_roflcat_reset_color(p->reset);
}
static void _roflcat_lookup_color_forward(ROFLCAT_printer* p, wchar_t c){
	if(c == L'\n'){
		_roflcat_lookup_inc_line(p);
		if(p->invert)
			_roflcat_lookup_reset(p);
		return;
	}else{
		int width = wcwidth(c);
		if(width > 0)
			_roflcat_lookup_inc_col(p, width);
		else
			_roflcat_lookup_inc_col(p, 1);
	}
	long new_index = (((long)(p->freq*p->lookup.column + p->spread*p->lookup.line + p->randomness))%p->lookup.table_size);
	if(new_index < 0)
		new_index += p->lookup.table_size;
	if(new_index != p->lookup.index){
		p->lookup.index = new_index;
		wprintf(p->color_string, _roflcat_rgb_lookup_table[p->lookup.table[p->lookup.index]]);
	}
}
static void _roflcat_roflcat_color_inc(ROFLCAT_color* c){
	switch(c->target){
	default:
	case ROFLCAT_TC_TARGET_R:
		c->target = ROFLCAT_TC_TARGET_G;
		break;
	case ROFLCAT_TC_TARGET_G:
		c->target = ROFLCAT_TC_TARGET_B;
		break;
	case ROFLCAT_TC_TARGET_B:
		c->target = ROFLCAT_TC_TARGET_R;
		break;
	};
}
static void _roflcat_roflcat_color_dec(ROFLCAT_color* c){
	switch(c->target){
	default:
	case ROFLCAT_TC_TARGET_R:
		c->target = ROFLCAT_TC_TARGET_B;
		break;
	case ROFLCAT_TC_TARGET_G:
		c->target = ROFLCAT_TC_TARGET_R;
		break;
	case ROFLCAT_TC_TARGET_B:
		c->target = ROFLCAT_TC_TARGET_G;
		break;
	};
}
static void _roflcat_rgb_do_inc_color(ROFLCAT_printer* p, int amount, ROFLCAT_color* color){
	int* curr = NULL, *next = NULL, *prev = NULL;
	void(*inc)(ROFLCAT_color*) = NULL;
	if(amount > 0){
		inc = _roflcat_roflcat_color_inc;
		switch(color->target){
		default:
		case ROFLCAT_TC_TARGET_R:
			curr = &color->r;
			next = &color->g;
			prev = &color->b;
			break;
		case ROFLCAT_TC_TARGET_G:
			curr = &color->g;
			next = &color->b;
			prev = &color->r;
			break;
		case ROFLCAT_TC_TARGET_B:
			curr = &color->b;
			next = &color->r;
			prev = &color->g;
			break;
		};
	}else{
		inc = _roflcat_roflcat_color_dec;
		switch(color->target){
		default:
		case ROFLCAT_TC_TARGET_R:
			curr = &color->b;
			next = &color->g;
			prev = &color->r;
			break;
		case ROFLCAT_TC_TARGET_G:
			curr = &color->r;
			next = &color->b;
			prev = &color->g;
			break;
		case ROFLCAT_TC_TARGET_B:
			curr = &color->g;
			next = &color->r;
			prev = &color->b;
			break;
		};
	}
	amount = _roflcat_abs(amount);
	while(amount != 0){
		int old = *curr;
		*curr = _roflcat_min((*curr)+amount, p->truecolor.color_max);
		amount = (old+amount)-(*curr);
		old = *prev;
		*prev = _roflcat_max((*prev)-amount, p->truecolor.color_min);
		amount = (amount-old)+(*prev);
		if((*prev) == p->truecolor.color_min){
			inc(color);
			int* tmp = curr;
			curr = next;next = prev;prev = tmp;
		}
	}
}
static void _roflcat_rgb_inc_line(ROFLCAT_printer* p){
	int amount = p->spread*(((float)p->truecolor.color_max)/4);
	_roflcat_rgb_do_inc_color(p, amount, &p->truecolor.line_color);
	_roflcat_copy_color(&p->truecolor.color, &p->truecolor.line_color);
}
static int _roflcat_rgb_inc_col(ROFLCAT_printer* p, int i){
	int amount = i*p->freq*(((float)p->truecolor.color_max)/4);
	if(!amount)
		return 0;
	_roflcat_rgb_do_inc_color(p, amount, &p->truecolor.color);
	return 1;
}
static void _roflcat_rgb_color_forward(ROFLCAT_printer* p, wchar_t c){
	int updated = 0;
	if(c == L'\n'){
		_roflcat_rgb_inc_line(p);
		if(p->invert)
			_roflcat_rgb_reset(p);
		return;
	}else{
		int width = wcwidth(c);
		if(width > 0)
			updated = _roflcat_rgb_inc_col(p, width);
		else
			updated = _roflcat_rgb_inc_col(p, 1);
	}
	if(updated){
		wprintf(p->color_string, _roflcat_rgb_lookup_table[p->truecolor.color.r],
		                         _roflcat_rgb_lookup_table[p->truecolor.color.g],
		                         _roflcat_rgb_lookup_table[p->truecolor.color.b]);
	}
}


//returns number of characters printed
static int _roflcat_print_lookup_char(ROFLCAT_printer* p, wchar_t c){
	_roflcat_lookup_color_forward(p, c);
	if(putwchar(c) == WEOF)
		return 0;
	return 1;
}
static int _roflcat_print_mono_char(ROFLCAT_printer* p, wchar_t c){
	UNUSED_VARIABLE(p);
	if(putwchar(c) == WEOF)
    return 0;
  return 1;
}
static int _roflcat_print_rgb_char(ROFLCAT_printer* p, wchar_t c){
	_roflcat_rgb_color_forward(p, c);
	if(putwchar(c) == WEOF)
		return 0;
	return 1;
}

//Printing API
int roflcat_print_wchar(ROFLCAT_printer* p, wchar_t c, roflcat_print_fun print_fn){
	if(p->squeeze_blanks){
		if(p->had_newline){
			if(c == L'\n'){
				if(p->is_squeezing == 0){
					++p->is_squeezing;
				}else{
					return 0;
				}
			}else{
				p->is_squeezing = 0;
			}
		}
	}
	if((p->number_lines != ROFLCAT_NUMBER_LINES_NEVER && p->had_newline) && (p->number_lines == ROFLCAT_NUMBER_LINES_ALWAYS || c != L'\n')){
		wchar_t buffer[24];
		swprintf(buffer, sizeof(buffer)/sizeof(buffer[0])-1, L"%" ROFLCAT_LINE_NUMBERING_SPACE "d ", p->line_num++);
		for(wchar_t* ch = buffer;*ch != 0;++ch){
      print_fn(p, *ch);
		}
		print_fn(p, L' ');
	}
	if(c == L'\n'){
		p->had_newline = 1;
		if(p->show_ends){
			for(const wchar_t* ch = p->line_end_str;*ch;++ch){
	      print_fn(p, *ch);
			}
    }
	}else{
		p->had_newline = 0;
	}
	if(c == L'\t'){
		for(const wchar_t* ch = p->tab_str;*ch;++ch){
      print_fn(p, *ch);
		}
	}else{
		if(p->show_nonprinting){
			char tmp[5];
			_roflcat_nonprinting_notation(c, tmp);
			for(char* t = tmp;*t;++t)
				print_fn(p, c);
		}else{
    	print_fn(p, c);
		}
	}
	return 1;
}
int roflcat_print_wstring(ROFLCAT_printer* p, const wchar_t* str, roflcat_print_fun custom_fn){
	int retval = 0;
	switch(p->type){
	case ROFLCAT_PRINTER_TYPE_LOOKUP:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, _roflcat_print_lookup_char);
		}
		break;
	case ROFLCAT_PRINTER_TYPE_MONO:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, _roflcat_print_mono_char);
		}
		break;
	case ROFLCAT_PRINTER_TYPE_RGB:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, _roflcat_print_rgb_char);
		}
		break;
	case ROFLCAT_PRINTER_TYPE_CUSTOM:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, custom_fn);
		}
		break;
	};
	return retval;
}
int roflcat_print_char(ROFLCAT_printer* p, char c, roflcat_print_fun print_fn){
	return roflcat_print_wchar(p, c, print_fn);
}
//yes it's the same function as print_wstring, but this is actually important to be separate
int roflcat_print_string(ROFLCAT_printer* p, const char* str, roflcat_print_fun custom_fn){
	int retval = 0;
	switch(p->type){
	case ROFLCAT_PRINTER_TYPE_LOOKUP:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, _roflcat_print_lookup_char);
		}
		break;
	case ROFLCAT_PRINTER_TYPE_MONO:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, _roflcat_print_mono_char);
		}
		break;
	case ROFLCAT_PRINTER_TYPE_RGB:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, _roflcat_print_rgb_char);
		}
		break;
	case ROFLCAT_PRINTER_TYPE_CUSTOM:
		for(;*str;++str){
			retval += roflcat_print_wchar(p, *str, custom_fn);
		}
		break;
	};
	return retval;
}
void roflcat_reset_color(ROFLCAT_printer* p){
	wprintf(L"%ls", p->reset);
}
